all:phobia.love

phobia.love:phobia/*.lua phobia/data/debris/* phobia/data/enemy/* phobia/data/gui/* phobia/data/levels/* phobia/data/music/* phobia/data/player/* phobia/data/sound/* phobia/data/*
	rm -f phobia.love
	cd phobia && zip -r ../phobia.love * && cd -

run_package:phobia.love
	love phobia.love

run:
	love phobia
