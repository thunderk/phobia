======================
Phobia Revival Project
======================

Description
-----------

This is an open-source rewrite attempt of the game `Phobia III : Edge of Humanity`_, created by RedLynx Labs.
It's a top-down, fast-paced, survival/shooter game.

Although the original game is available for Windows and Linux at no cost, the source code is not open and outdated Linux version does not run on modern distibutions.

This project will try to reimplement the game engine, using original data at first.

.. _`Phobia III : Edge of Humanity`: http://www.redlynx.com/phobiaIII/

Requirements
------------

The only dependency needed to run the game is the fantastic Löve_ engine.

.. _Löve: http://love2d.org/

Build / Run
-----------

The Löve_ engine needs a packaged file phobia.love to run the game.

You can either download an already packaged phobia.love, if one is available on GitHub_ or BitBucket_, or build your own.

On Linux systems, you can use the command 'make' in the repository root to build it.
You can then run the game with 'love phobia.love'.

Alternatively, you can run the sources without building the package, with the command 'love phobia' from the repository root.

On Windows systems, you need to download a packaged phobia.love file and double-click on it.

.. _GitHub: https://github.com/thunderk/phobia
.. _BitBucket: https://bitbucket.org/thunderk/phobia

How to play
-----------

The goal is to survive each level, by annihilating waves after waves of dreadful alien creatures.

Key mappings :

- Arrow keys : move and turn
- Space bar : fire
- p : Toggle pause
- F4 : Toggle fullscreen

On some keyboards/systems, arrow key strokes may be ignored when pressing too much keys. Alternative mappings for movement are provided to circumvent this : either use the keypad (4,8,6,2 or 4,8,6,5), or the (d,r,g,f) combo.

Licensing
---------

For source code and modified data, see the LICENSE file.

For original data, please refer to the `original website`__.

__ `Phobia III : Edge of Humanity`_
