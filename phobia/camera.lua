-- Camera management
local camera = {}

local level = require "level"
local player = require "player"

applied = false
center_x = 0.0
center_y = 0.0
scaling = 1.0
ideal_center_x = 0.0
ideal_center_y = 0.0
ideal_scaling = 1.0
level_width = 1
level_height = 1
window_width = 1
window_height = 1

function camera.reset()
    -- Reset the camera at initial position (full level visible)
    level_width = level.getWidth()
    level_height = level.getHeight()
    window_width = love.graphics.getWidth()
    window_height = love.graphics.getHeight()

    center_x = level_width / 2
    center_y = level_height / 2
    scaling = 0.5
end

function camera.update(dt)
    -- Update ideal camera position
    local player_location = player.getLocation()
    ideal_center_x = player_location['x']
    ideal_center_y = player_location['y']
    ideal_scaling = 1.0

    if player.isDead() then
        ideal_scaling = 1.5
    end

    -- Update camera limits
    if ideal_center_x * ideal_scaling - window_width * 0.5 < 0 then
        ideal_center_x = window_width * 0.5 / ideal_scaling
    end
    if ideal_center_x * ideal_scaling + window_width * 0.5 >= level_width * ideal_scaling then
        ideal_center_x = (level_width * ideal_scaling - window_width * 0.5) / ideal_scaling
    end
    if ideal_center_y * ideal_scaling - window_height * 0.5 < 0 then
        ideal_center_y = window_height * 0.5 / ideal_scaling
    end
    if ideal_center_y * ideal_scaling + window_height * 0.5 >= level_height * ideal_scaling then
        ideal_center_y = (level_height * ideal_scaling - window_height * 0.5) / ideal_scaling
    end

    -- Update real camera position
    if dt > 1.0 then
        dt = 1.0
    end
    center_x = center_x + (ideal_center_x - center_x) * dt
    center_y = center_y + (ideal_center_y - center_y) * dt
    scaling = scaling + (ideal_scaling - scaling) * dt
end

function camera.apply()
    -- Apply camera position
    --  Do it at draw() start
    if applied then
        love.graphics.pop()
    else
        applied = true
    end

    love.graphics.push()
    love.graphics.translate(window_width / 2 - center_x * scaling, window_height / 2 - center_y * scaling)
    love.graphics.scale(scaling)
end

return camera
