local debris = {}

debris.definition = {}
debris.dynamic = {}
debris.tostatic = {}
debris.static = nil

function debris.load()
    -- Load debris definition from data
    debris.definition = {}
    for i, fname in ipairs(love.filesystem.enumerate("data/debris")) do
        local enemy = string.gmatch(fname, "%a+")()
        local image = love.graphics.newImage("data/debris/" .. fname)
        if debris.definition[enemy] then
            table.insert(debris.definition[enemy], image)
        else
            debris.definition[enemy] = {image}
        end
    end
end

function debris.reset()
    -- Clear all debris sprites
    local level = require "level"
    debris.dynamic = {}
    debris.tostatic = {}
    debris.static = love.graphics.newCanvas(level.getWidth(), level.getHeight())
end

function debris.update(dt)
    -- Time update
    for i, part in pairs(debris.dynamic) do
        if dt > part.lifetime then
            ndt = part.lifetime
        else
            ndt = dt
        end
        part.lifetime = part.lifetime - ndt
        part.x = part.x + part.dx * ndt
        part.y = part.y + part.dy * ndt
        part.orientation = part.orientation + part.rotation * ndt

        if part.lifetime <= 0 then
            table.remove(debris.dynamic, i)
            debris.tostatic[#(debris.tostatic) + 1] = part
        end
    end
end

function debris.drawBottom()
    -- Move debris to canvas
    if #debris.tostatic > 0 then
        local camera = require "camera"

        love.graphics.pop()
        love.graphics.push()
        love.graphics.setCanvas(debris.static)

        for i, part in ipairs(debris.tostatic) do
            love.graphics.setColor(135, 135, 135)
            love.graphics.draw(part.image, part.x, part.y, part.orientation, part.scaling, part.scaling, part.image:getWidth() / 2, part.image:getHeight() / 2)
        end

        love.graphics.setCanvas()
        camera.apply()

        debris.tostatic = {}
    end

    -- Draw static debris canvas
    love.graphics.setColor(255, 255, 255)
    love.graphics.draw(debris.static)
end

function debris.drawTop()
    -- Draw debris on screen
    for i, part in ipairs(debris.dynamic) do
        local value = 255
        if part.lifetime < 1 then
            value = value - (1.0 - part.lifetime) * 120.0
        end
        love.graphics.setColor(value, value, value)
        love.graphics.draw(part.image, part.x, part.y, part.orientation, part.scaling, part.scaling, part.image:getWidth() / 2, part.image:getHeight() / 2)
    end

    -- Restore color
    love.graphics.setColor(255, 255, 255)
end

function debris.createBodyParts(x, y, enemy, direction, multiplier, power)
    -- Create body parts for a dead enemy
    local def = debris.definition[enemy]
    if power == nil then
        power = 150.0
    end
    if multiplier == nil then
        multiplier = 1
    end
    if def then
        for i=1, multiplier do
            for i, partdef in ipairs(def) do
                local part = {}
                part.x = x
                part.y = y
                part.orientation = 0
                part.dx = math.random() - 0.5
                part.dy = math.random() - 0.5
                if direction ~= nil then
                    part.dx = part.dx + math.sin(direction)
                    part.dy = part.dy - math.cos(direction)
                end
                part.dx = power * part.dx
                part.dy = power * part.dy
                part.rotation = 30 * (math.random() - 0.5)
                part.scaling = 1.1
                part.image = partdef
                part.lifetime = math.random() * 0.5 + 0.5
                debris.dynamic[#(debris.dynamic) + 1] = part
            end
        end
    end
end

return debris
