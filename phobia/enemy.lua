local enemy = {}

local sprite = require "sprite"
local debris = require "debris"
local physics = require "physics"
local player = require "player"

enemies = {}
snd_die = nil

function enemy.load()
    snd_die = love.audio.newSource("data/sound/enemy_death.ogg", "static")
end

function enemy.clear()
    enemies = {}
end

function enemy.add(x, y, heading, type)
    -- Add a living enemy
    instance = {}
    instance.enemy = true
    instance.damage = 0.0
    instance.life = 5.0
    instance.x = x
    instance.y = y
    instance.heading = heading
    instance.type = type
    instance.animstate = 0.0 -- animation progress
    instance.physics = physics.addObject(15, x, y, instance)
    instance.deviation = math.random() * math.pi * 2.0
    instance.last_hit_direction = 0.0

    physics.setSlowDown(instance.physics, 0.01, 0.7)

    enemies[#enemies + 1] = instance
end

function enemy.update(dt)
    for i, instance in pairs(enemies) do
        if instance.damage >= instance.life then
            -- Enemy death
            love.audio.play(snd_die)
            debris.createBodyParts(instance.x, instance.y, instance.type, instance.last_hit_direction)
            physics.removeObject(instance.physics)
            table.remove(enemies, i)

            collectgarbage()
        end

        local speed = 1.2 * physics.getEffectiveSlowDown(instance.physics)

        instance.x = instance.x + math.sin(instance.heading) * speed
        instance.y = instance.y - math.cos(instance.heading) * speed
        instance.animstate = math.fmod(instance.animstate + 0.1, 1.0)

        instance.deviation = instance.deviation + dt

        -- Get direction to player
        local player_location = player.getLocation()
        local heading_to_player = math.atan2(player_location.x - instance.x, instance.y - player_location.y)
        heading_to_player = heading_to_player + 0.8 * math.sin(instance.deviation)

        -- Head toward player
        while instance.heading - heading_to_player > math.pi do
            instance.heading = instance.heading - math.pi * 2.0
        end
        while heading_to_player - instance.heading > math.pi do
            instance.heading = instance.heading + math.pi * 2.0
        end
        if instance.heading > heading_to_player + 0.1 then
            instance.heading = instance.heading - 0.03
        elseif instance.heading < heading_to_player - 0.1 then
            instance.heading = instance.heading + 0.03
        end

        physics.setObjectLocation(instance.physics, instance.x, instance.y)
    end
end

function enemy.draw()
    for i=1, #enemies do
        local instance = enemies[i]
        sprite.draw(instance.type, "moving", instance.animstate, instance.x, instance.y, instance.heading)
    end
end

function enemy.drawDebug()
    for i=1, #enemies do
        local instance = enemies[i]
        love.graphics.setColor(0, 0, 255, 128)
        love.graphics.line(instance.x - 10, instance.y + 10, instance.x - 10 + (instance.life - instance.damage) * 20 / instance.life, instance.y + 10)
        love.graphics.setColor(255, 255, 255, 255)
    end
end

return enemy
