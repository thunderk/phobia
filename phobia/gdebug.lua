local debug = {}

debug.enabled = false
debug.lines = {}

function debug.toggle()
    -- Toggle debug mode
    debug.enabled = not debug.enabled
end

function debug.draw()
    -- Draw debug info on screen
    if debug.enabled then
        love.graphics.setLineWidth(2, "smooth")
        for i, line in ipairs(debug.lines) do
            if line.duration > 0.1 then
                love.graphics.setColor(255, 0, 0, 255)
            else
                love.graphics.setColor(255, 0, 0, math.ceil(2555.0 * line.duration))
            end
            love.graphics.line(line.x1, line.y1, line.x2, line.y2)
        end
        love.graphics.setColor(255, 255, 255, 255)
    end
end

function debug.update(dt)
    -- Time update
    if debug.enabled then
        for i, line in pairs(debug.lines) do
            line.duration = line.duration - dt
            if line.duration < 0 then
                table.remove(debug.lines, i)
            end
        end
    end
end

function debug.addLine(x1, y1, x2, y2, duration)
    -- Add a debug line
    if debug.enabled then
        line = {}
        line.x1 = x1
        line.y1 = y1
        line.x2 = x2
        line.y2 = y2
        line.duration = duration

        debug.lines[#debug.lines + 1] = line
    end
end

return debug
