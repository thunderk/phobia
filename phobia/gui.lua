local gui = {}

local player = require "player"

infobar = nil
infolife = nil
player_health = 0.0

function gui.load(number)
    infobar = love.graphics.newImage(string.format("data/gui/infobar.png", number))
    infolife = love.graphics.newImage(string.format("data/gui/infolife.png", number))
end

function gui.update(dt)
    player_health = player.getHealth()
end

function gui.draw()
    -- Draw background
    love.graphics.draw(infobar, 0, 0)

    -- Draw health
    if player_health then
        for i = 0, math.floor(65.0 * player_health) do
            love.graphics.draw(infolife, 25 + i * 6, 8)
        end
    end
end

return gui
