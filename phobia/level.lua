local level = {}

local enemy = require "enemy"
local tools = require "tools"

level_background = nil
level_walls = nil
level_waves = {}
level_progress = 0.0

function level.load(number)
    level_background = love.graphics.newImage(string.format("data/levels/%02d_background.png", number))
    level_walls = love.graphics.newImage(string.format("data/levels/%02d_walls.png", number))

    love.audio.stop()
    local music = love.audio.newSource(string.format("data/music/level%02d.ogg", number))
    love.audio.play(music)

    -- Load enemy sequence
    level_progress = 0.0
    enemy.clear()
    level_waves = {}
    for i, parts in ipairs(tools.readData(string.format("data/levels/%02d_sequence", number))) do
        local wave = {}
        wave.start = tonumber(parts[1])
        wave.stop = tonumber(parts[2])
        wave.count = tonumber(parts[3])
        wave.enemy = parts[4]
        wave.area = parts[5]

        wave.interval = (wave.stop - wave.start) / wave.count
        wave.done = 0
        wave.lastcall = 0.0

        level_waves[#level_waves + 1] = wave
    end
end

function level.update(dt)
    for i, wave in pairs(level_waves) do
        if level_progress >= wave.start and level_progress <= wave.stop then
            local due = math.floor((level_progress - wave.start) / wave.interval)
            if due > wave.done then
                for i = 1, due - wave.done do
                    local x = 0.0
                    local y = 0.0
                    local area = wave.area

                    if area == "borders" then
                        local borders = {"east", "west", "south", "north"}
                        local border = math.random(4)
                        area = borders[border]
                    end

                    if area == "east" then
                        x = level.getWidth()
                        y = math.random(level.getHeight())
                    elseif area == "west" then
                        x = 0
                        y = math.random(level.getHeight())
                    elseif area == "north" then
                        y = 0
                        x = math.random(level.getWidth())
                    elseif area == "south" then
                        y = level.getHeight()
                        x = math.random(level.getWidth())
                    end

                    enemy.add(x, y, math.random() * math.pi * 2.0, wave.enemy)
                end
                wave.done = due
            end
        end
    end

    level_progress = level_progress + dt
end

function level.draw()
    love.graphics.draw(level_background, 0, 0)
    love.graphics.draw(level_walls, 0, 0)
end

function level.getWidth()
    return level_background:getWidth()
end

function level.getHeight()
    return level_background:getHeight()
end

return level
