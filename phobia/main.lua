local physics = require "physics"
local level = require "level"
local sprite = require "sprite"
local player = require "player"
local enemy = require "enemy"
local debris = require "debris"
local obstacles = require "obstacles"
local camera = require "camera"
local gui = require "gui"
local debug = require "gdebug"

current_level = 0
debug_mode = false
pause_mode = false

function changeLevel(nb)
    current_level = nb
    physics.reset()
    level.load(current_level)
    obstacles.load(current_level)
    camera.reset()
    debris.reset()
    player.reset()
end

function love.load()
    font_medium = love.graphics.newFont(18)

    love.graphics.setCaption("Phobia Revival")
    love.mouse.setVisible(false)

    sprite.load()
    player.load()
    enemy.load()
    debris.load()
    gui.load()
    changeLevel(0)
end

function love.keypressed(key)
    if key == "escape" then
        love.event.push("quit")
    elseif key == "f2" then
        changeLevel(current_level)
    elseif key == "f3" then
        changeLevel(current_level + 1)
    elseif key == "f4" then
        love.graphics.toggleFullscreen()
    elseif key == "f11" then
        player.die()
    elseif key == "f12" then
        debug.toggle()
        debug_mode = not debug_mode
    elseif key == "p" or key == "pause" then
        pause_mode = not pause_mode
    end
end

function love.draw()
    camera.apply()
    level.draw()
    debris.drawBottom()
    player.draw()
    enemy.draw()
    debris.drawTop()

    if debug_mode then
        debug.draw()
        obstacles.drawDebug()
        physics.drawDebug()
        enemy.drawDebug()
    end

    love.graphics.pop()
    love.graphics.push()
    gui.draw()

    if pause_mode then
        love.graphics.setFont(font_medium)
        love.graphics.setColor(0, 0, 0)
        love.graphics.print("Paused (hit 'p' to resume)", 52, 51)
        love.graphics.setColor(230, 60, 50)
        love.graphics.print("Paused (hit 'p' to resume)", 50, 50)
        love.graphics.setColor(255, 255, 255)
    end
end

function love.update(dt)
    if not pause_mode and love.graphics.hasFocus() then
        physics.update(dt)
        camera.update(dt)
        level.update(dt)
        player.update(dt)
        enemy.update(dt)
        debris.update(dt)
        gui.update(dt)

        if debug_mode then
            debug.update(dt)
        end
    end
end

