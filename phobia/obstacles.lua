local obstacles = {}

local tools = require "tools"
local physics = require "physics"

obstacles.objects = {}

function obstacles.load(level)
    obstacles.objects = {}
    for i, parts in ipairs(tools.readData(string.format("data/levels/%02d_obstacles", level))) do
        local obstacle = {}
        obstacle.obstacle = true
        obstacle.x = tonumber(parts[1])
        obstacle.y = tonumber(parts[2])
        obstacle.w = tonumber(parts[3])
        obstacle.h = tonumber(parts[4])

        obstacle.physics = physics.addStatic(obstacle.x, obstacle.y, obstacle.w, obstacle.h, obstacle)
        physics.setSlowDown(obstacle.physics, 100)

        table.insert(obstacles.objects, obstacle)
    end
end

function obstacles.drawDebug()
    for i, obstacle in ipairs(obstacles.objects) do
        love.graphics.setColor(50, 50, 50, 100)
        love.graphics.rectangle("fill", obstacle.x, obstacle.y, obstacle.w, obstacle.h)
        love.graphics.setColor(255, 255, 255, 255)
    end
end

return obstacles
