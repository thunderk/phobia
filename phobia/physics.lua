local physics = {}

local debug = require "gdebug"

physics.world = love.physics.newWorld()

physics.next_id = 0
physics.contacts = {}
physics.todelete = {}

function beginContact(f1, f2, coll)
    -- Process a new contact between two fixtures
    coll = nil

    local info1 = f1:getUserData()
    local info2 = f2:getUserData()
    local contacts = physics.contacts

    local contact_info = contacts[info1.id]
    if contact_info == nil then
        contact_info = {}
        contact_info.count = 1
        contact_info.slowdown = info2.slowdown_given
        contacts[info1.id] = contact_info
    else
        contact_info.count = contact_info.count + 1
        contact_info.slowdown = contact_info.slowdown + info2.slowdown_given
    end

    contact_info = contacts[info2.id]
    if contact_info == nil then
        contact_info = {}
        contact_info.count = 1
        contact_info.slowdown = info1.slowdown_given
        contacts[info2.id] = contact_info
    else
        contact_info.count = contact_info.count + 1
        contact_info.slowdown = contact_info.slowdown + info1.slowdown_given
    end
end

function endContact(f1, f2, coll)
    -- Process a contact ending between two fixtures
    coll = nil

    local info1 = f1:getUserData()
    local info2 = f2:getUserData()
    local contacts = physics.contacts

    local contact_info = contacts[info1.id]
    if contact_info == nil then
        contact_info = {}
        contact_info.count = 0
        contact_info.slowdown = 0
        contacts[info1.id] = contact_info
    else
        contact_info.count = contact_info.count - 1
        if contact_info.count == 0 then
            contact_info.slowdown = 0
        else
            contact_info.slowdown = contact_info.slowdown - info2.slowdown_given
        end
    end

    contact_info = contacts[info2.id]
    if contact_info == nil then
        contact_info = {}
        contact_info.count = 0
        contact_info.slowdown = 0
        contacts[info2.id] = contact_info
    else
        contact_info.count = contact_info.count - 1
        if contact_info.count == 0 then
            contact_info.slowdown = 0
        else
            contact_info.slowdown = contact_info.slowdown - info1.slowdown_given
        end
    end

    collectgarbage()
end

function physics.reset()
    -- Reset the physics world, clearing any object
    physics.contacts = {}
    physics.todelete = {}

    physics.world:destroy()

    physics.world = love.physics.newWorld()
    physics.world:setCallbacks(beginContact, endContact)

    physics.next_id = 0
end

function physics.update(dt)
    -- Update physics simulation
    physics.world:update(dt)
    collectgarbage()

    -- Process deletions
    if #physics.todelete > 0 then
        for i, fixture_todelete in ipairs(physics.todelete) do
            fixture_todelete:destroy()
        end

        physics.todelete = {}
        collectgarbage()
    end
end

function physics.drawDebug()
    -- Draw debug info on screen
    for i, body in ipairs(physics.world:getBodyList()) do
        local x, y = body:getPosition()

        local fixture = body:getFixtureList()[1]
        if fixture then
            local collisions = physics.getCollisions(fixture)
            if collisions > 0 then
                if collisions > 10 then
                    collisions = 10
                end
                love.graphics.setColor(155 + 10 * collisions, 255, 0, 100)
            else
                love.graphics.setColor(0, 255, 0, 100)
            end

            local shape = body:getFixtureList()[1]:getShape()
            if shape:typeOf("CircleShape") then
                love.graphics.circle("fill", x, y, shape:getRadius())
            else
                local x1, y1, x2, y2 = fixture:getBoundingBox()
                love.graphics.rectangle("fill", x1, y1, x2 - x1, y2 - y1)
            end
            love.graphics.setColor(255, 255, 255, 255)
        end
    end
end

function physics.addObject(radius, x, y, parent)
    -- Create an object in the physics world, returning a reference to its fixture

    local object = love.physics.newBody(physics.world, x, y, "dynamic")
    local shape = love.physics.newCircleShape(radius)
    local fixture = love.physics.newFixture(object, shape)

    fixture:setSensor(true)

    local user_data = {}
    user_data.id = physics.next_id
    user_data.parent = parent
    user_data.slowdown_given = 0
    user_data.slowdown_received = 0
    fixture:setUserData(user_data)

    physics.next_id = physics.next_id + 1

    return fixture
end

function physics.addStatic(x, y, w, h, parent)
    -- Create a static object in the physics world, returning a reference to its fixture

    local object = love.physics.newBody(physics.world, x + w / 2, y + h / 2, "dynamic")
    local shape = love.physics.newRectangleShape(w, h)
    local fixture = love.physics.newFixture(object, shape)

    fixture:setSensor(true)

    local user_data = {}
    user_data.id = physics.next_id
    user_data.parent = parent
    user_data.slowdown_given = 0
    user_data.slowdown_received = 0
    fixture:setUserData(user_data)

    physics.next_id = physics.next_id + 1

    return fixture
end

function physics.removeObject(obj)
    physics.todelete[#physics.todelete + 1] = obj
end

function physics.setObjectLocation(object, x, y)
    -- Change the location of an object in the physics world
    object:getBody():setPosition(x, y)
end

function physics.setSlowDown(object, given, received)
    -- Set the slow down an object has on others, and the maximum it can receive
    local data = object:getUserData()
    data.slowdown_given = given
    data.slowdown_received = received
end

function physics.getBulletHit(start_x, start_y, heading, category)
    -- Get the first object hit by a bullet (return its parent)
    local hit
    local hit_x = start_x + math.sin(heading) * 1000.0
    local hit_z = start_y - math.cos(heading) * 1000.0
    local hit_dist = 1000000.0
    debug.addLine(start_x, start_y, hit_x, hit_z, 0.1)
    physics.world:rayCast(start_x, start_y, hit_x, hit_z, function(fixture, x, y, xn, yn, fraction)
        local nhit_dist = (x - start_x) * (x - start_x) + (y - start_y) * (y - start_y)
        local parent = fixture:getUserData().parent
        if nhit_dist < hit_dist then
            if (not category) or parent[category] then
                hit = fixture:getUserData().parent
                hit_dist = nhit_dist
            end
        end
        return 1
    end)
    return hit
end

function physics.getCollisions(obj)
    -- Get the number of collisions between an object and other objects

    local obj_id = obj:getUserData().id

    local info = physics.contacts[obj_id]
    if info ~= nil and info.count > 0 then
        return info.count
    else
        return 0
    end
end

function physics.getEffectiveSlowDown(obj)
    -- Get the resulting slowdown of collisions (factor to apply to speed)

    local obj_id = obj:getUserData().id

    local max = obj:getUserData().slowdown_received

    if max <= 0 then
        return 1.0
    end

    local info = physics.contacts[obj_id]
    if info ~= nil and info.slowdown > 0 then
        local slowdown = info.slowdown
        if slowdown > max then
            slowdown = max
        end

        return 1.0 - slowdown
    else
        return 1.0
    end
end

return physics
