local player = {}

local sprite = require "sprite"
local physics = require "physics"
local debris = require "debris"

speed_movement = 120.0
speed_rotation = 3.0
anim_moving_factor = 0.04

snd_fire = nil

physics_object = nil

location_x = 0.0
location_y = 0.0

heading = 0.0
sprite_anim_progress = 0.0

is_forward = false
is_backward = false
is_turning = false
is_firing = false

health = 1.0
alive = true
last_hit = nil

function player.load()
    snd_fire = love.audio.newSource("data/sound/gun01.ogg", "static")
end

function player.reset()
    location_x = 500.0
    location_y = 500.0
    heading = 0.0
    physics_object = physics.addObject(15, location_x, location_y)
    physics.setSlowDown(physics_object, 0.001, 0.8)
    health = 1.0
    alive = true

    local level = require "level"
    level_width = level.getWidth()
    level_height = level.getHeight()
end

function player.getLocation()
    return {['x']=location_x, ['y']=location_y}
end

function player.getHealth()
    -- Return the player health, in 0.0-1.0 range
    return health
end

function player.isDead()
    -- Return true if the player isDead
    return not alive
end

function player.draw()
    local anim
    if not alive then
        return
    elseif is_firing then
        anim = "firing"
    elseif is_forward or is_backward or is_turning then
        anim = "moving"
    else
        anim = "static"
    end
    love.graphics.setColor(0, 0, 0, 120)
    sprite.draw("player", anim, sprite_anim_progress, location_x + 6, location_y + 3, heading)
    love.graphics.setColor(255, 255, 255, 255)
    sprite.draw("player", anim, sprite_anim_progress, location_x, location_y, heading)
end

function player.die()
    -- Trigger player death
    alive = false
    local direction = nil
    local force = 200
    if is_forward then
        direction = heading
    elseif is_backward then
        direction = heading + math.pi
        force = force * 0.5
    end
    debris.createBodyParts(location_x, location_y, "player", direction, 4, force)
    physics.removeObject(physics_object)
end

function player.update(dt)
    if not alive then
        return
    elseif health <= 0 then
        player.die()
        return
    end

    -- Apply speed penalties
    local final_speed_movement = speed_movement
    local final_speed_rotation = speed_rotation
    if is_forward then
        final_speed_rotation = final_speed_rotation * 0.9
    elseif is_backward then
        final_speed_rotation = final_speed_rotation * 0.6
        final_speed_movement = final_speed_movement * 0.4
    end
    if is_turning then
        final_speed_movement = final_speed_movement * 0.8
    end
    if is_firing then
        final_speed_movement = final_speed_movement * 0.7
    end
    final_speed_movement = final_speed_movement * physics.getEffectiveSlowDown(physics_object)

    -- Apply movement from keyboard
    if love.keyboard.isDown("up") or love.keyboard.isDown("r") or love.keyboard.isDown("kp8") then
        location_x = location_x + final_speed_movement * dt * math.sin(heading)
        location_y = location_y - final_speed_movement * dt * math.cos(heading)
        is_forward = true
        is_backward = false
    elseif love.keyboard.isDown("down") or love.keyboard.isDown("f") or love.keyboard.isDown("kp5") or love.keyboard.isDown("kp2") then
        location_x = location_x - final_speed_movement * dt * math.sin(heading)
        location_y = location_y + final_speed_movement * dt * math.cos(heading)
        is_forward = false
        is_backward = true
    else
        is_forward = false
        is_backward = false
    end
    if love.keyboard.isDown("left") or love.keyboard.isDown("d") or love.keyboard.isDown("kp4") then
        heading = heading - final_speed_rotation * dt
        is_turning = true
    elseif love.keyboard.isDown("right") or love.keyboard.isDown("g") or love.keyboard.isDown("kp6") then
        heading = heading + final_speed_rotation * dt
        is_turning = true
    else
        is_turning = false
    end
    is_firing = love.keyboard.isDown(" ")


    -- Stay in level
    if location_x < 0 then
        location_x = 0
    elseif location_x > level_width then
        location_x = level_width
    end
    if location_y < 0 then
        location_y = 0
    elseif location_y > level_height then
        location_y = level_height
    end

    -- Animate sprite
    if is_firing then
        sprite_anim_progress = math.fmod(sprite_anim_progress + dt * 8.0, 1.0)
    else
        sprite_anim_progress = math.fmod(sprite_anim_progress + dt * final_speed_movement * anim_moving_factor, 1.0)
    end

    -- Sounds
    if is_firing then
        love.audio.play(snd_fire)
    else
        love.audio.stop(snd_fire)
    end

    -- Weapon effect
    if is_firing then
        -- TODO Control the firing rate
        local direction = heading - 0.95
        local hit = physics.getBulletHit(location_x + math.sin(heading) * 8.0, location_y - math.cos(heading) * 8.0, direction, "enemy")
        if hit and hit.damage then
            local distance = math.sqrt((location_x - hit.x) * (location_x - hit.x) + (location_y - hit.y) * (location_y - hit.y))
            if distance <= 100 then
                hit.damage = hit.damage + 1.0
            elseif distance >= 500 then
                hit.damage = hit.damage + 0.1
            else
                hit.damage = hit.damage + 0.1 + 0.9 * (1.0 - (distance - 100.0) / 400.0)
            end
            hit.last_hit_direction = direction
        end
    end

    -- Physics
    physics.setObjectLocation(physics_object, location_x, location_y)

    -- Check hit by enemy
    if physics.getCollisions(physics_object) > 0 then
        now = love.timer.getMicroTime()
        if last_hit == nil or now - last_hit > 0.1 then
            last_hit = now
            health = health - 0.01
        end
    end

end

return player
