-- Sprite management, with animations
local sprite = {}

local definitions = {}

function getKey(type, animation)
    return string.format("%s_%s", type, animation)
end

function sprite.load()
    -- Load definition file
    for line in love.filesystem.lines("data/sprites") do
        local params = {}
        local images = {}
        for part in string.gmatch(line, "%S+") do
            if #params < 2 then
                table.insert(params, part)
            else
                table.insert(images, love.graphics.newImage(part))
            end
        end
        definitions[getKey(params[1], params[2])] = images
    end
end

function sprite.draw(type, animation, progress, x, y, heading)
    -- Draw a sprite at an animation point (defined by progress)
    local def = definitions[getKey(type, animation)]
    local picnb = math.floor(1.0 + progress * #def)
    if picnb > #def then
        picnb = #def
    end
    love.graphics.draw(def[picnb], x, y, heading, 1, 1, def[picnb]:getWidth() / 2, def[picnb]:getHeight() / 2)
end

return sprite
