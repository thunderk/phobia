local tools = {}

function tools.readData(filepath)
    local results = {}
    if love.filesystem.isFile(filepath) then
        for line in love.filesystem.lines(filepath) do
            if string.len(line) > 0 and string.sub(line, 1, 1) ~= "#" then
                result = {}
                for part in string.gmatch(line, "%S+") do
                    result[#result + 1] = part
                end
                results[#results + 1] = result
            end
        end
    end
    return results
end

return tools
